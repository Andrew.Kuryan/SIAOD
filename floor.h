//
// Created by andrew on 18.09.18.
//

#ifndef SIAOD_FLOOR_H
#define SIAOD_FLOOR_H

#include "gtkcell.h"
#include "base_gtk_elements/gtkpoint.h"
#include "base_gtk_elements/gtkpolygon.h"

#define floor_size 10
#define RIGHT_TOP 1
#define LEFT_TOP 2
#define RIGHT_BOTTOM 3
#define LEFT_BOTTOM 4

typedef struct _Floor Floor;
typedef struct _PathField PathField;

struct _Floor {
    int num;
    char* title;
    GtkPoint* abs;

    GtkCell *cell[floor_size][floor_size];
    int exits_num;
    GtkPoint* exits[floor_size * floor_size];
    GtkPolygon* path;

    GtkBox* box;
    GtkFixed* field;
};

struct _PathField{
    int x, y;
    int num_of_steps;
    int flag_visit;
    GtkPoint* path[floor_size * floor_size];
};

Floor* floor_new(const char* t, int x_a, int y_a);
void floor_set_configuration(Floor* floor, char matr[floor_size][floor_size]);
void floor_put_step(Floor* floor, int i, int j);
void floor_put_exit(Floor* floor, int i, int j, int exit_type);
void floor_put_room(Floor* floor, const char* name, int exit_pos, int x_pos, int y_pos);
GtkPoint* floor_print_path(Floor* floor, GtkPoint* start);

#endif //SIAOD_FLOOR_H
