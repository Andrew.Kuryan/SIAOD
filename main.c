#include <stdio.h>
#include <stdlib.h>
#include "floor.h"
#include "gtkcell.h"
#include <gtk/gtk.h>
#include <math.h>
#include "base_gtk_elements/gtkline.h"
#include "base_gtk_elements/gtkpolygon.h"
#include "building.h"

const char matr1[floor_size][floor_size] = {{'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'},
                                              {'s', '0', '0', '0', '0', 's', '0', '0', '0', 's'},
                                              {'s', '0', '0', '0', '0', 's', '0', '0', '0', 's'},
                                              {'s', '0', '0', '0', '0', 's', '0', '0', '0', 's'},
                                              {'s', '0', '0', '0', '0', 's', '0', '0', '0', 's'},
                                              {'s', '0', '0', '0', '0', 's', 's', 's', 's', '3'},
                                              {'s', '0', '0', '0', '0', 's', '0', '0', '0', 's'},
                                              {'s', '0', '0', '0', '0', 's', '0', '0', '0', 's'},
                                              {'s', '0', '0', '0', '0', 's', '0', '0', '0', 's'},
                                              {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'}};

const char matr2[floor_size][floor_size] = {{'2', 's', 's', 's', 's', 's', 's', 's', 's', '1'},
                                            {'s', '0', '0', '0', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', '0', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', '0', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', '0', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', '0', '0', '0', '0', '0', '0', 's'},
                                            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'},
                                            {'s', '0', '0', '0', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', '0', '0', '0', '0', '0', '0', 's'},
                                            {'1', 's', 's', 's', 's', 's', 's', 's', 's', '2'}};

const char matr3[floor_size][floor_size] = {{'2', 's', 's', 's', 's', 's', 's', 's', 's', '1'},
                                            {'s', '0', '0', 's', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', 's', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', 's', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', 's', '0', '0', '0', '0', '0', 's'},
                                            {'s', '0', '0', 's', 's', 's', 's', 's', 's', 's'},
                                            {'s', '0', '0', 's', '0', 's', '0', '0', '0', 's'},
                                            {'s', '0', '0', 's', '0', 's', '0', '0', '0', 's'},
                                            {'s', '0', '0', 's', '0', 's', '0', '0', '0', 's'},
                                            {'1', 's', 's', 's', 's', 's', 's', 's', 's', '2'}};

int main (int argc, char *argv[]) {

    gtk_init (&argc, &argv);

    GtkWindow* window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
    gtk_window_set_title(window, "Задача 1");
    gtk_widget_set_size_request(GTK_WIDGET(window), 800, 500);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    Building* building = building_new();
    building_add_floor(building);
    floor_set_configuration(building_get_floor(building, 0), matr1);
    gtk_cell_set_configuration(building_get_floor(building, 0)->cell[0][0], LADDER);
    gtk_cell_set_configuration(building_get_floor(building, 0)->cell[0][9], LIFT);
    gtk_cell_set_configuration(building_get_floor(building, 0)->cell[9][0], LIFT);
    gtk_cell_set_configuration(building_get_floor(building, 0)->cell[9][9], LADDER);

    building_add_floor(building);
    floor_set_configuration(building_get_floor(building, 1), matr2);
    floor_put_room(building_get_floor(building, 1), "Камера 1", LEFT_TOP, 2, 1);
    floor_put_room(building_get_floor(building, 1), "ПУ", LEFT_BOTTOM, 8, 8);

    building_add_floor(building);
    floor_set_configuration(building_get_floor(building, 2), matr3);
    floor_put_room(building_get_floor(building, 2), "Лабор-я", RIGHT_BOTTOM, 6, 7);
    floor_put_room(building_get_floor(building, 2), "Камера 2", RIGHT_TOP, 1, 4);

    building_set.buildings[0] = building;

    GtkFixed* fixed = GTK_FIXED(gtk_fixed_new());

    gtk_fixed_put(fixed, GTK_WIDGET(building->grid), 0, 0);
    gtk_widget_show(GTK_WIDGET(fixed));

    gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(fixed));
    gtk_widget_show(GTK_WIDGET(window));

    gtk_main ();
    return 0;
}