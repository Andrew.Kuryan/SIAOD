//
// Created by andrew on 26.09.18.
//

#ifndef SIAOD_BUILDING_H
#define SIAOD_BUILDING_H

#include "floor.h"

#define MARGIN_LEFT 30
#define MARGIN_TOP 20
#define MARGIN_RIGHT 30
#define X_SPACING 50
#define Y_SPACING 50

typedef struct _Building Building;

struct _BuildingSet{
    Building* buildings[10];
}building_set;

struct _Building{
    int num_of_floors;
    Floor* floors[10];

    GtkGrid* grid;
};

Building* building_new();
void building_add_floor(Building* building);
void connect_signal(Building* building, Floor* floor);
Floor* building_get_floor(Building* building, int num);

#endif //SIAOD_BUILDING_H
