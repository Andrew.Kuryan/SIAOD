//
// Created by andrew on 18.09.18.
//

#include <stdlib.h>
#include <string.h>
#include "floor.h"
#include "base_gtk_elements/gtkpolygon.h"

Floor* floor_new(const char* t, int x_a, int y_a){
    Floor* floor = (Floor*) calloc(1, sizeof(Floor));
    floor->box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 20));
    floor->title = (char*) calloc(strlen(t), sizeof(char));
    strcpy(floor->title, t);
    floor->abs = gtk_point_new(x_a, y_a);
    for (int i=0; i<floor_size; i++){
        for (int j=0; j<floor_size; j++){
            floor->cell[i][j] = NULL;
        }
    }
    GtkLabel* label = GTK_LABEL(gtk_label_new(t));
    floor->field = GTK_FIXED(gtk_fixed_new());
    gtk_box_pack_start(floor->box, GTK_WIDGET(label), gtk_false(), gtk_false(), 0);
    gtk_box_pack_start(floor->box, GTK_WIDGET(floor->field), gtk_false(), gtk_false(), 0);

    gtk_widget_show(GTK_WIDGET(label));
    gtk_widget_show(GTK_WIDGET(floor->field));
    gtk_widget_show(GTK_WIDGET(floor->box));

    return floor;
}

void floor_put_step(Floor* floor, int i, int j){
    GtkCell* cell = gtk_cell_new(floor->num, i, j, i*cell_size, j*cell_size);
    floor->cell[i][j] = cell;
    gtk_fixed_put(floor->field, GTK_WIDGET(cell->button), cell->abs->x, cell->abs->y);
}

void floor_put_exit(Floor* floor, int i, int j, int exit_type){
    GtkCell* cell = gtk_cell_new(floor->num, i, j, i*cell_size, j*cell_size);
    gtk_cell_set_configuration(cell, exit_type);
    floor->cell[i][j] = cell;
    floor->exits[floor->exits_num] = gtk_point_new(i, j);
    floor->exits_num++;
    gtk_fixed_put(floor->field, GTK_WIDGET(cell->button), cell->abs->x, cell->abs->y);
}

//1 2
//3 4
void floor_put_room(Floor* floor, const char* name, int exit_pos, int x_pos, int y_pos){
    int x_abs = 0, y_abs = 0;
    GtkCell* cell;
    switch (exit_pos){
        case RIGHT_TOP:
            x_abs = x_pos;
            y_abs = y_pos;
            break;
        case LEFT_TOP:
            x_abs = x_pos-1;
            y_abs = y_pos;
            break;
        case RIGHT_BOTTOM:
            x_abs = x_pos;
            y_abs = y_pos-1;
            break;
        case LEFT_BOTTOM:
            x_abs = x_pos-1;
            y_abs = y_pos-1;
            break;
        default:
            g_print("Error in parsing room");
    }
    cell = gtk_cell_new(floor->num, x_pos, y_pos, x_pos*cell_size, y_pos*cell_size);
    cell->name = (char*) calloc(strlen(name), sizeof(char));
    strcpy(cell->name, name);
    gtk_cell_set_configuration(cell, ROOM);
    floor->cell[x_pos][y_pos] = cell;
    gtk_fixed_put(floor->field, GTK_WIDGET(cell->button), x_abs*cell_size, y_abs*cell_size);
}

PathField* path_queue_new(Floor* floor, int i, int j){
    PathField* path_queue = (PathField*) calloc(1, sizeof(PathField));
    path_queue->x = i;
    path_queue->y = j;
    path_queue->flag_visit = 0;
    path_queue->num_of_steps = 0;
    path_queue->path[0] = gtk_point_new(floor->cell[i][j]->abs->x + cell_size / 2,
            floor->cell[i][j]->abs->y + cell_size / 2);
    return path_queue;
}

void pathcopy(GtkPoint** dest, GtkPoint** arr, int num){
    for (int i=0; i<num; i++){
        dest[i] = arr[i];
    }
}

void floor_set_configuration(Floor* floor, char matr[floor_size][floor_size]){
    for (int i=0; i<floor_size; i++){
        for (int j=0; j<floor_size; j++){
            if (matr[i][j] >= '1' && matr[i][j] <= '3')
                floor_put_exit(floor, i, j, ((int) matr[i][j]-48));
            else if (matr[i][j] == 's')
                floor_put_step(floor, i, j);
        }
    }
}

GtkPoint* floor_print_path(Floor* floor, GtkPoint* start){
    PathField* path_field[floor_size][floor_size];
    for (int i=0; i<floor_size; i++){
        for (int j=0; j<floor_size; j++){
            if (floor->cell[i][j] != NULL)
                path_field[i][j] = path_queue_new(floor, i, j);
            else
                path_field[i][j] = NULL;
        }
    }
    PathField* queue[floor_size * floor_size];
    int q_begin, q_end;
    queue[0] = path_field[start->x][start->y];
    path_field[start->x][start->y]->flag_visit = 1;
    q_begin = 0;
    q_end = 1;
    while (q_begin < q_end){
        if (queue[q_begin]->x+1 < floor_size && path_field[queue[q_begin]->x+1][queue[q_begin]->y] != NULL
                && path_field[queue[q_begin]->x+1][queue[q_begin]->y]->flag_visit == 0){
            queue[q_end] = path_field[queue[q_begin]->x+1][queue[q_begin]->y];
            path_field[queue[q_begin]->x+1][queue[q_begin]->y]->flag_visit = 1;
            path_field[queue[q_begin]->x+1][queue[q_begin]->y]->num_of_steps =
                    path_field[queue[q_begin]->x][queue[q_begin]->y]->num_of_steps + 1;

            pathcopy(path_field[queue[q_begin]->x+1][queue[q_begin]->y]->path,
                     path_field[queue[q_begin]->x][queue[q_begin]->y]->path,
                     path_field[queue[q_begin]->x][queue[q_begin]->y]->num_of_steps+1);

            path_field[queue[q_begin]->x+1][queue[q_begin]->y]->
                    path[path_field[queue[q_begin]->x+1][queue[q_begin]->y]->num_of_steps] =
                    gtk_point_new(floor->cell[queue[q_begin]->x+1][queue[q_begin]->y]->abs->x + cell_size/2,
                                  floor->cell[queue[q_begin]->x+1][queue[q_begin]->y]->abs->y + cell_size/2);
            q_end++;
        }
        if (queue[q_begin]->x-1 >= 0     &&     path_field[queue[q_begin]->x-1][queue[q_begin]->y] != NULL
                && path_field[queue[q_begin]->x-1][queue[q_begin]->y]->flag_visit == 0){
            queue[q_end] = path_field[queue[q_begin]->x-1][queue[q_begin]->y];
            path_field[queue[q_begin]->x-1][queue[q_begin]->y]->flag_visit = 1;
            path_field[queue[q_begin]->x-1][queue[q_begin]->y]->num_of_steps =
                    path_field[queue[q_begin]->x][queue[q_begin]->y]->num_of_steps + 1;

            pathcopy(path_field[queue[q_begin]->x-1][queue[q_begin]->y]->path,
                     path_field[queue[q_begin]->x][queue[q_begin]->y]->path,
                     path_field[queue[q_begin]->x][queue[q_begin]->y]->num_of_steps+1);

            path_field[queue[q_begin]->x-1][queue[q_begin]->y]->
                    path[path_field[queue[q_begin]->x-1][queue[q_begin]->y]->num_of_steps] =
                    gtk_point_new(floor->cell[queue[q_begin]->x-1][queue[q_begin]->y]->abs->x + cell_size/2,
                                  floor->cell[queue[q_begin]->x-1][queue[q_begin]->y]->abs->y + cell_size/2);
            q_end++;
        }
        if (queue[q_begin]->y-1 >= 0     &&     path_field[queue[q_begin]->x][queue[q_begin]->y-1] != NULL
                && path_field[queue[q_begin]->x][queue[q_begin]->y-1]->flag_visit == 0){
            queue[q_end] = path_field[queue[q_begin]->x][queue[q_begin]->y-1];
            path_field[queue[q_begin]->x][queue[q_begin]->y-1]->flag_visit = 1;
            path_field[queue[q_begin]->x][queue[q_begin]->y-1]->num_of_steps =
                    path_field[queue[q_begin]->x][queue[q_begin]->y]->num_of_steps + 1;

            pathcopy(path_field[queue[q_begin]->x][queue[q_begin]->y-1]->path,
                     path_field[queue[q_begin]->x][queue[q_begin]->y]->path,
                     path_field[queue[q_begin]->x][queue[q_begin]->y]->num_of_steps+1);

            path_field[queue[q_begin]->x][queue[q_begin]->y-1]->
                    path[path_field[queue[q_begin]->x][queue[q_begin]->y-1]->num_of_steps] =
                    gtk_point_new(floor->cell[queue[q_begin]->x][queue[q_begin]->y-1]->abs->x + cell_size/2,
                                  floor->cell[queue[q_begin]->x][queue[q_begin]->y-1]->abs->y + cell_size/2);
            q_end++;
        }
        if (queue[q_begin]->y+1 < floor_size && path_field[queue[q_begin]->x][queue[q_begin]->y+1] != NULL
                && path_field[queue[q_begin]->x][queue[q_begin]->y+1]->flag_visit == 0){
            queue[q_end] = path_field[queue[q_begin]->x][queue[q_begin]->y+1];
            path_field[queue[q_begin]->x][queue[q_begin]->y+1]->flag_visit = 1;
            path_field[queue[q_begin]->x][queue[q_begin]->y+1]->num_of_steps =
                    path_field[queue[q_begin]->x][queue[q_begin]->y]->num_of_steps + 1;

            pathcopy(path_field[queue[q_begin]->x][queue[q_begin]->y+1]->path,
                     path_field[queue[q_begin]->x][queue[q_begin]->y]->path,
                     path_field[queue[q_begin]->x][queue[q_begin]->y]->num_of_steps+1);

            path_field[queue[q_begin]->x][queue[q_begin]->y+1]->
                    path[path_field[queue[q_begin]->x][queue[q_begin]->y+1]->num_of_steps] =
                    gtk_point_new(floor->cell[queue[q_begin]->x][queue[q_begin]->y+1]->abs->x + cell_size / 2,
                                  floor->cell[queue[q_begin]->x][queue[q_begin]->y+1]->abs->y + cell_size / 2);
            q_end++;
        }
        q_begin++;
    }
    int min_path = 0;
    for (int i=0; i<floor->exits_num; i++){
        if (path_field[floor->exits[i]->x][floor->exits[i]->y]->num_of_steps <
                path_field[floor->exits[min_path]->x][floor->exits[min_path]->y]->num_of_steps)
            min_path = i;
    }
    int x_test = floor->exits[min_path]->x, y_test = floor->exits[min_path]->y;
    path_field[x_test][y_test]->path[path_field[x_test][y_test]->num_of_steps] =
            gtk_point_new(floor->cell[x_test][y_test]->abs->x + cell_size /2
                    , floor->cell[x_test][y_test]->abs->y + cell_size /2);
    path_field[x_test][y_test]->num_of_steps++;
    floor->path = gtk_polygon_new(path_field[x_test][y_test]->num_of_steps, path_field[x_test][y_test]->path);
    gtk_fixed_put(floor->field, GTK_WIDGET(floor->path->field), 0, 0);
    return gtk_point_new(x_test, y_test);
}