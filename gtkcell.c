//
// Created by andrew on 23.09.18.
//

#include <stdlib.h>
#include "gtkcell.h"
#include "handler.h"

GtkCell* gtk_cell_new(int floor, int x_p, int y_p, int x_a, int y_a){
    GtkCell* gtk_cell = (GtkCell*) calloc(1, sizeof(GtkCell));
    gtk_cell->floor_num = floor;

    gtk_cell->button = GTK_BUTTON(gtk_button_new());
    gtk_widget_set_size_request(GTK_WIDGET(gtk_cell->button), cell_size, cell_size);

    gtk_cell->pos = gtk_point_new(x_p, y_p);
    gtk_cell->abs = gtk_point_new(x_a, y_a);
    g_signal_connect_swapped(G_OBJECT(gtk_cell->button), "clicked", G_CALLBACK(execute), handler_new(gtk_cell->floor_num, gtk_point_new(x_p, y_p)));
    gtk_widget_show(GTK_WIDGET(gtk_cell->button));
    return gtk_cell;
}

void gtk_cell_set_configuration(GtkCell* cell, int conf){
    GtkImage* image;
    switch (conf){
        case LIFT:
            image = GTK_IMAGE(gtk_image_new_from_file("../resources/images/lift.png"));
            gtk_widget_set_visible(GTK_WIDGET(image), gtk_true());
            gtk_button_set_image(cell->button, GTK_WIDGET(image));
            break;
        case LADDER:
            image = GTK_IMAGE(gtk_image_new_from_file("../resources/images/ladder.png"));
            gtk_widget_set_visible(GTK_WIDGET(image), gtk_true());
            gtk_button_set_image(cell->button, GTK_WIDGET(image));
            break;
        case DOOR:
            image = GTK_IMAGE(gtk_image_new_from_file("../resources/images/door.png"));
            gtk_widget_set_visible(GTK_WIDGET(image), gtk_true());
            gtk_button_set_image(cell->button, GTK_WIDGET(image));
            break;
        case ROOM:
            g_print("name: %s\n", cell->name);
            gtk_button_set_label(cell->button, cell->name);
            gtk_widget_set_size_request(GTK_WIDGET(cell->button), cell_size * 2, cell_size * 2);
            break;
        default:
            g_print("Error in setting cell configuration\n");
    }
}