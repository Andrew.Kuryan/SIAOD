//
// Created by andrew on 26.09.18.
//

#include <stdlib.h>
#include <string.h>
#include "building.h"

Building* building_new(){
    Building* building = (Building*) calloc(1, sizeof(Building));
    building->grid = GTK_GRID(gtk_grid_new());

    gtk_grid_set_column_spacing(building->grid, X_SPACING);
    gtk_grid_set_row_spacing(building->grid, Y_SPACING);
    gtk_widget_set_margin_start(GTK_WIDGET(building->grid), MARGIN_LEFT);
    gtk_widget_set_margin_top(GTK_WIDGET(building->grid), MARGIN_TOP);
    gtk_widget_set_margin_end(GTK_WIDGET(building->grid), MARGIN_RIGHT);

    gtk_widget_show(GTK_WIDGET(building->grid));
    return building;
}

void building_add_floor(Building* building) {
    const char *fl_str = "Этаж";
    char *title = (char *) calloc(6, sizeof(char));
    title[0] = (char) (building->num_of_floors + 1 + 48);
    title[1] = ' ';
    strcat(title, fl_str);

    Floor* floor = floor_new(title, 0, 0);
    floor->num = building->num_of_floors;
    building->floors[building->num_of_floors] = floor;
    gtk_container_add(GTK_CONTAINER(building->grid), GTK_WIDGET(building->floors[building->num_of_floors]->box));
    building->num_of_floors++;
}

Floor* building_get_floor(Building* building, int num){
    return building->floors[num];
}