//
// Created by andrew on 24.09.18.
//

#ifndef SIAOD_GTKLINE_H
#define SIAOD_GTKLINE_H

#include <gtk/gtk.h>
#include "gtkpoint.h"

#define P_WIDTH 3
#define PEN_10PT_BROWN "../resources/images/point.png"
#define PEN_6PT_RED "../resources/images/point1.png"
#define STEP 1

typedef struct _GtkLine GtkLine;

struct _GtkLine{
    GtkFixed* field;
};

GtkLine* gtk_line_new(GtkPoint* start, GtkPoint* end);
void gtk_line_destroy(GtkLine* line);

#endif //SIAOD_GTKLINE_H
