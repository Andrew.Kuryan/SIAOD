//
// Created by andrew on 25.09.18.
//

#include <stdlib.h>
#include "gtkpolygon.h"

GtkPolygon* gtk_polygon_new(int num, GtkPoint* points[num]){
    GtkPolygon* polygon = (GtkPolygon*) calloc(1, sizeof(GtkPolygon));
    polygon->num_of_vertex = num;
    polygon->field = GTK_FIXED(gtk_fixed_new());

    polygon->lines = (GtkLine**) calloc((size_t) num, sizeof(GtkLine*));
    GtkPoint* prev_point = points[0];
    for (int i=1; i<num; i++){
        polygon->lines[i-1] = gtk_line_new(prev_point, points[i]);
        gtk_fixed_put(polygon->field, GTK_WIDGET(polygon->lines[i-1]->field), 0, 0);
        prev_point = points[i];
    }

    gtk_widget_show(GTK_WIDGET(polygon->field));
    return polygon;
}

void gtk_polygon_destroy(GtkPolygon* polygon){
    for (int i=0; i<polygon->num_of_vertex-1; i++)
        gtk_line_destroy(polygon->lines[i]);
    free(polygon->lines);
    gtk_widget_destroy(GTK_WIDGET(polygon->field));
    free(polygon);
}