//
// Created by andrew on 24.09.18.
//

#include <stdlib.h>
#include <math.h>
#include "gtkline.h"

GtkLine* gtk_line_new(GtkPoint* start, GtkPoint* end){
    GtkLine* line = (GtkLine*) calloc(1, sizeof(GtkLine));
    line->field = GTK_FIXED(gtk_fixed_new());

    int x1 = start->x, y1 = start->y,
        x2 = end->x, y2 = end->y;
    if (abs(x2-x1) >= abs(y2-y1)) {
        double yStep = STEP * (y2 - y1) / (double) abs(x2 - x1);
        double yInter = (double) y1;
        int xStep = x2 > x1 ? STEP : -STEP;
        while (x1 != x2) {
            GtkImage *point = GTK_IMAGE(gtk_image_new_from_file(PEN_6PT_RED));
            gtk_widget_set_visible(GTK_WIDGET(point), gtk_true());
            gtk_fixed_put(line->field, GTK_WIDGET(point), x1 - P_WIDTH, (int) (yInter - P_WIDTH));
            yInter += yStep;
            x1 += xStep;
        }
    }
    else{
        double xStep = STEP * (x2 - x1) / (double) abs(y2 - y1);
        double xInter = (double) x1;
        int yStep = y2 > y1 ? STEP : -STEP;
        while (y1 != y2) {
            GtkImage *point = GTK_IMAGE(gtk_image_new_from_file(PEN_6PT_RED));
            gtk_widget_set_visible(GTK_WIDGET(point), gtk_true());
            gtk_fixed_put(line->field, GTK_WIDGET(point), (int) (xInter - P_WIDTH), y1 - P_WIDTH);
            xInter += xStep;
            y1 += yStep;
        }
    }
    gtk_widget_show(GTK_WIDGET(line->field));

    return line;
}

void gtk_line_destroy(GtkLine* line){
    GList *children, *iter;
    children = gtk_container_get_children(GTK_CONTAINER(line->field));
    for(iter = children; iter != NULL; iter = g_list_next(iter))
        gtk_widget_destroy(GTK_WIDGET(iter->data));
    g_list_free(children);
    gtk_widget_destroy(GTK_WIDGET(line->field));
    free(line);
}