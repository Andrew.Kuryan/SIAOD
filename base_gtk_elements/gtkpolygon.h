//
// Created by andrew on 25.09.18.
//

#ifndef SIAOD_GTKBROKENLINE_H
#define SIAOD_GTKBROKENLINE_H

#include <gtk/gtk.h>
#include "gtkpoint.h"
#include "gtkline.h"

typedef struct _GtkPolygon GtkPolygon;

struct _GtkPolygon{
    GtkFixed* field;
    int num_of_vertex;
    GtkLine* *lines;
};

GtkPolygon* gtk_polygon_new(int num, GtkPoint* points[num]);
void gtk_polygon_destroy(GtkPolygon* polygon);

#endif //SIAOD_GTKBROKENLINE_H
