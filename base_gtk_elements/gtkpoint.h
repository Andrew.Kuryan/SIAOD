//
// Created by andrew on 24.09.18.
//

#ifndef SIAOD_GTKPOINT_H
#define SIAOD_GTKPOINT_H

typedef struct _GtkPoint GtkPoint;

struct _GtkPoint{
    int x, y;
};

GtkPoint* gtk_point_new(int x, int y);

#endif //SIAOD_GTKPOINT_H
