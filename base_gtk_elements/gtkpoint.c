//
// Created by andrew on 24.09.18.
//

#include <stdlib.h>
#include "gtkpoint.h"

GtkPoint* gtk_point_new(int x, int y){
    GtkPoint* point = (GtkPoint*) calloc(1, sizeof(GtkPoint));
    point->x = x;
    point->y = y;
    return point;
}