//
// Created by andrew on 26.09.18.
//

#include <gtk/gtk.h>
#include <stdlib.h>
#include "handler.h"
#include "building.h"

Handler* handler_new(int num, GtkPoint* point){
    Handler* handler = (Handler*) calloc(1, sizeof(Handler));
    handler->num_float = num;
    handler->point = point;
    return handler;
}

void execute(Handler* handler){
    for (int i = building_set.buildings[0]->num_of_floors-1; i>=0; i--){
        if (building_get_floor(building_set.buildings[0], i)->path != NULL) {
            gtk_polygon_destroy(building_get_floor(building_set.buildings[0], i)->path);
            building_get_floor(building_set.buildings[0], i)->path = NULL;
        }
    }
    GtkPoint* point = gtk_point_new(handler->point->x, handler->point->y);
    for (int i=handler->num_float; i>=0; i--){
        point = floor_print_path(building_get_floor(building_set.buildings[0], i), point);
    }
}