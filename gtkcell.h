//
// Created by andrew on 23.09.18.
//

#ifndef SIAOD_GTKCELL_H
#define SIAOD_GTKCELL_H

#include <gtk/gtk.h>
#include "base_gtk_elements/gtkpoint.h"

#define cell_size 30

#define LIFT 1
#define LADDER 2
#define DOOR 3
#define ROOM 4

typedef struct _GtkCell GtkCell;

struct _GtkCell{
    int floor_num;
    char* name;
    GtkButton* button;
    GtkPoint* abs;
    GtkPoint* pos;
};

GtkCell* gtk_cell_new(int floor, int x_a, int y_a, int x_p, int y_p);
void gtk_cell_set_configuration(GtkCell* cell, int conf);

#endif //SIAOD_GTKCELL_H
