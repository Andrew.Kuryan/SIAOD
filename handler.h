//
// Created by andrew on 26.09.18.
//

#ifndef SIAOD_HANDLER_H
#define SIAOD_HANDLER_H

#include "base_gtk_elements/gtkpoint.h"

typedef struct _Handler Handler;

struct _Handler{
    int num_float;
    GtkPoint* point;
};

Handler* handler_new(int num, GtkPoint* point);
void execute(Handler* handler);

#endif //SIAOD_HANDLER_H
